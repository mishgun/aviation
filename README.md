# Aviation - frontend application example #

### How do I get set up? ###

* Install bower and grunt
* Once they have been installed go to the directory and install the grunt-cli -> 'npm install -g grunt-cli' and then 'npm install'
* Next run 'bower install' to install all the dependencies required
* Once everything has been installed simply run 'grunt server'
* Open localhost:9000 in your browser
* Find details on About page