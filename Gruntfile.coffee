modRewrite = require('connect-modrewrite')

module.exports = (grunt) ->

  require('load-grunt-tasks')(grunt)

  grunt.initConfig

    haml:
      html:
        expand: true
        cwd: "src"
        src: "**/*.haml"
        ext: ".html"
        dest: "web"
        options:
          language: "coffee"
          target: "html"

      jst:
        expand: true
        cwd: "src"
        src: "**/*.hamlc"
        ext: ".js"
        dest: "web"
        options:
          language: "coffee"
          target: "js"
          placement: "amd"

    sass:
      styles:
        expand: true
        cwd: "src"
        src: "**/*.scss"
        ext: ".css"
        dest: "web"
        options:
          sourceMap: true

    coffee:
      scripts:
        expand: true
        cwd: "src"
        src: "**/*.coffee"
        ext: ".js"
        dest: "web"
        options:
          bare: true

    copy:
      data:
        expand: true
        cwd: "src"
        src: ["data/**", "images/**"]
        dest: "web"
      dist:
        expand: true
        cwd: "web"
        src: ["data/**", "images/**", "styles/**", "vendor/requirejs/**", "index.html"]
        dest: "dist"

    clean:
      web: ["web/*", "!web/vendor"]
      dist: ["dist"]

    watch:
      options:
        livereload: true
      scripts:
        files: "src/**/*.coffee"
        tasks: "coffee"
      styles:
        files: "src/**/*.scss"
        tasks: "sass"
      html:
        files: "src/**/*.haml"
        tasks: "haml:html"
      jst:
        files: "src/**/*.hamlc"
        tasks: "haml:jst"

    connect:
      server:
        options:
          port: 9000
          hostname: "*"
          base: "web"
          open: false
          middleware: (connect, options, middlewares) ->
            middlewares.unshift modRewrite([
              "\\airflights$ /data/airflights.json [L]"
              "\\aircrafts$ /data/aircrafts.json [L]"
              "\\airports$ /data/airports.json [L]"
              "\\world_map$ /data/world-map.geo.json [L]"
              "!\\.html|\\.js|\\.svg|\\.css|\\.png|\\.gif$ /index.html [L]"
            ])
            middlewares
      dist:
        options:
          port: 9000
          hostname: "*"
          base: "dist"
          open: false
          middleware: "<%= connect.server.options.middleware %>"
          keepalive: true

    requirejs:
      app:
        options:
          preserveLicenseComments: false
          mainConfigFile: "web/scripts/bootstrap.js"
          baseUrl: "web/scripts"
          include: "bootstrap"
          out: "dist/scripts/bootstrap.js"
      404:
        options:
          preserveLicenseComments: false
          mainConfigFile: "web/scripts/bootstrap.js"
          baseUrl: "web/scripts"
          include: "views/pages/404"
          exclude: ["bootstrap"]
          out: "dist/scripts/views/pages/404.js"
      about:
        options:
          preserveLicenseComments: false
          mainConfigFile: "web/scripts/bootstrap.js"
          baseUrl: "web/scripts"
          include: "views/pages/about"
          exclude: ["bootstrap"]
          out: "dist/scripts/views/pages/about.js"
      index:
        options:
          preserveLicenseComments: false
          mainConfigFile: "web/scripts/bootstrap.js"
          baseUrl: "web/scripts"
          include: "views/pages/index"
          exclude: ["bootstrap"]
          out: "dist/scripts/views/pages/index.js"

  grunt.registerTask "server", [
    "clean:web"
    "copy:data"
    "sass"
    "coffee"
    "haml"
    "connect:server"
    "watch"
  ]

  grunt.registerTask "dist", [
    "clean"
    "sass"
    "coffee"
    "haml"
    "copy"
    "requirejs"
    "connect:dist"
  ]
