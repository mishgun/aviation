define [
  "Backbone"
  "Marionette"
  "views/top_countries/index"
  "views/top_cities/index"
  "views/popular_airports/index"
  "views/popular_airports_map/index"
  "views/highest_airports/index"
  "views/top_aircrafts/index"
], (
  Backbone
  Marionette
  topCountriesView
  topCitiesView
  popularAirportsView
  popularAirportsMapView
  highestAirportsView
  topAircraftsView
) ->

  class AviationStatsRouter extends Backbone.Router

    routes:
      "":                 "index"
      "about":            "about"
      "top_countries":    "topCountries"
      "top_cities":       "topCities"
      "busiest":          "popularAirports"
      "airports_map":     "popularAirportsMap"
      "highest":          "highestAirports"
      "aircrafts_top":    "topAircrafts"
      "*notFound":        "notFound"

    index: ->
      require ["views/pages/index"], (indexView) =>
        @showView new indexView

    about: ->
      require ["views/pages/about"], (aboutView) =>
        @showView new aboutView

    topCountries: ->
      @showView new topCountriesView

    topCities: ->
      @showView new topCitiesView

    popularAirports: ->
      @showView new popularAirportsView

    popularAirportsMap: ->
      @showView new popularAirportsMapView

    highestAirports: ->
      @showView new highestAirportsView

    topAircrafts: ->
      @showView new topAircraftsView

    notFound: ->
      require ["views/pages/404"], (pageNotFoundView) =>
        @showView new pageNotFoundView

    showView: (view) ->
      AviationStats.layout.content.show view
