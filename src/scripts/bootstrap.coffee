require.config
  paths:
    jquery: "../vendor/jquery/dist/jquery.min"
    underscore: "../vendor/underscore/underscore-min"
    backbone: "../vendor/backbone/backbone"
    Marionette: "../vendor/backbone.marionette/lib/backbone.marionette.min"
    highcharts: "../vendor/highcharts/highcharts"
    hcmaps: "../vendor/highcharts/modules/map"
    hcmore: "../vendor/highcharts/highcharts-more"
    proj4: "../vendor/proj4/dist/proj4"
  shim:
    "hcmore": ["highcharts"]
    "hcmaps": ["highcharts"]
  map:
    "*":
      "Backbone": "backbone"
      "Highcharts": "highcharts"

require ["app"], (AviationStats) ->
  window.AviationStats = new AviationStats
  window.AviationStats.start()
