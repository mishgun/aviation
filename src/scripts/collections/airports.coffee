define [
  "Backbone"
  "models/airport"
], (
  Backbone
  airportModel
) ->

  class airportsCollection extends Backbone.Collection
    url: "/airports"
    model: airportModel

    getTopCountries: (quantity=5) ->
      _.chain @models
        .groupBy (model) -> model.get "country"
        .map (key, value) ->
          country: value
          airports: key.length
        .sortBy (model) -> model.airports
        .slice -quantity
        .value()

    getTopCities: (quantity=5) ->
      _.chain @models
        .groupBy (model) -> "#{model.get('country')}##{model.get('city')}"
        .map (key, value) ->
          country: value.split("#")[0]
          city: value.split("#")[1]
          airports: key.length
        .sortBy (model) -> model.airports
        .slice -quantity
        .value()

    getHighestAirports: (quantity=5, shuffled=false) ->
      arr = _.chain @models
        .sortBy (model) -> model.get "altitude"
        .map (model) ->
          country: model.get "country"
          city: model.get "city"
          altitude: model.get "altitude"
        .slice -quantity
        .value()

      return arr unless shuffled
      @shuffle arr

    getFieldByCode: (field, code) ->
      @findWhere(airport_id: parseInt code)
      .get field

    getAirportDataByCode: (code) ->
      return if _.isString code
      @findWhere(airport_id: parseInt code).pick "airport_id", "name", "city", "country", "latitude", "longitude"

    shuffle: (arr) ->
      i = arr.length
      while --i > 0
        j = ~~(Math.random() * (i + 1))
        t = arr[j]
        arr[j] = arr[i]
        arr[i] = t
      arr
