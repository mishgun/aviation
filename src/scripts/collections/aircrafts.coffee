define [
  "Backbone"
  "models/aircraft"
], (
  Backbone
  aircraftModel
) ->

  class aircraftsCollection extends Backbone.Collection
    url: "/aircrafts"
    model: aircraftModel

    getByCode: (code) ->
      @findWhere code: code

    getNameByCode: (code) ->
      @findWhere(code: code)
      .get("name")