define [
  "Backbone"
  "models/flight"
], (
  Backbone
  flightModel
) ->

  class flightsCollection extends Backbone.Collection
    url: "/airflights"
    model: flightModel

    getPopularAircrafts: (quantity=5) ->
      aircrafts = {}

      _.chain @models
        .groupBy (model) -> model.get "equipment"
        .each (key, value) ->
          codes = value.toString().split " "
          _.each codes, (code) ->
            if aircrafts[code]
              aircrafts[code] += key.length
            else
              aircrafts[code] = key.length

      _.chain aircrafts
        .map (value, key) ->
          code: key
          flights: value
        .sortBy (model) ->
          model.flights
        .slice -quantity
        .map (model) ->
          aircraft_code: model.code
          aircraft_name: AviationStats.Data.aircrafts.getNameByCode model.code
          flights: model.flights
        .value()

    getPopularAirports: (quantity=5) ->
      airports = {}

      @each (model) ->
        _.each [
          model.get("source_airport_id")
          model.get("destination_airport_id")
        ], (id) ->
          if airports[id]
            airports[id] += 1
          else
            airports[id] = 1

      _.chain airports
        .map (value, key) ->
          code: key
          airports: value
        .sortBy (model) ->
          model.airports
        .slice -quantity
        .map (model) ->
          airport_code: model.code
          airport_name: AviationStats.Data.airports.getFieldByCode "name", model.code
          airport_city: AviationStats.Data.airports.getFieldByCode "city", model.code
          airport_country: AviationStats.Data.airports.getFieldByCode "country", model.code
          airport_latitude: AviationStats.Data.airports.getFieldByCode "latitude", model.code
          airport_longitude: AviationStats.Data.airports.getFieldByCode "longitude", model.code
          flights: model.airports
        .value()

    getHubDirections: (quantity=1) ->
      that = @
      airports = @getPopularAirports quantity

      _.map airports, (airport) ->
        destinations = that.where(source_airport_id: parseInt(airport.airport_code)).slice(0, 10)
        airport: airport
        directions: _.map destinations, (airport) ->
          AviationStats.Data.airports.getAirportDataByCode airport.get("destination_airport_id")
