define [
  "jquery"
  "Backbone"
  "Marionette"
  "views/layout"
  "routers/router"
  "collections/aircrafts"
  "collections/airports"
  "collections/flights"
], (
  $
  Backbone
  Marionette
  LayoutView
  AviationStatsRouter
  aircraftsCollection
  airportsCollection
  flightsCollection
) ->

  Marionette.Renderer.render = (template, data) ->
    if _.isFunction template
      template data
    else if template isnt false
      throw new Error '[Marionette Renderer]: Invalid template method'

  class AviationStats extends Marionette.Application

    onStart: ->
      @layout = new LayoutView
      @layout.render()

      @Data =
        aircrafts: new aircraftsCollection
        airports: new airportsCollection
        flights: new flightsCollection

      worldMap = $.ajax "/world_map"

      $.when @Data.aircrafts.fetch(), @Data.airports.fetch(), @Data.flights.fetch(), worldMap
      .then =>
        @Data.map = worldMap.responseJSON
        @router = new AviationStatsRouter
        Backbone.history.start pushState: true