define [
  "Marionette"
  "templates/pages/about"
], (
  Marionette
  templateAbout
) ->

  class aboutView extends Marionette.ItemView
    template: templateAbout