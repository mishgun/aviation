define [
  "Marionette"
  "templates/pages/404"
], (
  Marionette
  templateNotFound
) ->

  class pageNotFoundView extends Marionette.ItemView
    template: templateNotFound