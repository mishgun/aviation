define [
  "Marionette"
  "templates/pages/index"
  "views/top_countries/chart"
  "views/top_cities/chart"
  "views/popular_airports/chart"
  "views/popular_airports_map/chart"
  "views/highest_airports/chart"
  "views/top_aircrafts/chart"
], (
  Marionette
  templateIndex
  topCountriesChartView
  topCitiesChartView
  popularAirportsChartView
  popularAirportsMapView
  highestAirportsChartView
  topAircraftsChartView
) ->

  class indexView extends Marionette.LayoutView
    template: templateIndex

    regions:
      top_countries:    ".js-top-countries"
      top_cities:       ".js-top-cities"
      top_airports:     ".js-top-airports"
      top_airports_map: ".js-top-airports-map"
      highest_airports: ".js-highest-airports"
      top_aircrafts:    ".js-top-aircrafts"

    onShow: ->
      @top_countries.show new topCountriesChartView
        url: "/top_countries"
      @top_cities.show new topCitiesChartView
        url: "/top_cities"
      @top_airports.show new popularAirportsChartView
        url: "/busiest"
      @top_airports_map.show new popularAirportsMapView
        url: "/airports_map"
      @highest_airports.show new highestAirportsChartView
        url: "/highest"
      @top_aircrafts.show new topAircraftsChartView
        url: "/aircrafts_top"

      # console.log AviationStats.Data.flights.getHubDirections 1