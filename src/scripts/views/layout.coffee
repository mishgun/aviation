define [
  "jquery"
  "Backbone"
  "Marionette"
  "views/application/header"
 ], (
  $
  Backbone
  Marionette
  headerView
) ->

  class LayoutView extends Marionette.LayoutView
    template: false
    el: "body"

    regions:
      header: ".js-header"
      content: ".js-content"

    initialize: ->
      @$el.on "click", "a", @pushStateClick

    onRender: ->
      @header.show new headerView

    pushStateClick: (event) =>
      href = if $(event.target)[0].tagName is "A"
        $(event.target).attr("href")
      else
        $(event.target).closest("a").attr("href")

      return if $(event.target).attr("rel")
      return unless href
      return event.preventDefault() if href.charAt(0) == "#"

      @redirectUrl href
      false

    redirectUrl: (url, opts={trigger: true})->
      Backbone.history.navigate(url, opts)