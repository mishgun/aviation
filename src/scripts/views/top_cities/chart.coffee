define [
  "Marionette"
  "views/application/base_chart"
], (
  Marionette
  baseChartView
) ->

  class topCountriesChartView extends baseChartView

    getData: (quantity=5) ->
      data = AviationStats.Data.airports.getTopCities quantity
      _.map data, (model) -> ["#{model.city} (#{model.country})", model.airports]
