define [
  "Marionette"
  "proj4"
  "hcmaps"
], (
  Marionette
  proj4
) ->

  window.proj4 = proj4

  class popularAirportsMapView extends Marionette.ItemView
    className: "chart-wrap"
    template: false

    initialize: (options) ->
      quantity = options.quantity or 5
      @height = options.height or 250
      @data = @getData quantity
      @url = options.url or null

    onShow: ->
      @initChart()

    getData: (quantity=5) ->
      data = AviationStats.Data.flights.getPopularAirports quantity
      _.map data, (model) =>
        name: "#{model.airport_name} (#{model.airport_city}, #{model.airport_country})"
        lat: model.airport_latitude
        lon: model.airport_longitude
        z: model.flights
        city: model.airport_city

    initChart: ->
      @$el.highcharts 'Map',
        chart:
          height: @height
          backgroundColor: "rgba(255,255,255,0)"
          events:
            click: (event) =>
              AviationStats.layout.redirectUrl @url if @url
        title: false
        mapNavigation:
          enabled: true
        tooltip:
          headerFormat: '<b>{point.key}</b><br />'
          pointFormat: 'Flights: {point.z}'
        series: [{
          mapData: AviationStats.Data.map
          name: 'Basemap'
          borderColor: '#A0A0A0'
          nullColor: 'rgba(200, 200, 200, 0.3)'
          showInLegend: false
        },{
          type: 'mapbubble'
          name: 'Cities'
          showInLegend: false
          color: "#ff0000"
          data: @data
          minSize: 6
          maxSize: '5%'
          dataLabels:
            enabled: true
            style:
              fontSize: "10px"
              fontWeight: "normal"
              color: "#000"
            y: 10
            x: 0
            align: "center"
            format: "{point.city}"
        }]
