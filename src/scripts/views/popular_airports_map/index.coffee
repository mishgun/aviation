define [
  "Marionette"
  "templates/popular_airports_map/index"
  "views/popular_airports_map/chart"
], (
  Marionette
  templateIndex
  popularAirportsMapView
) ->

  class topCountriesView extends Marionette.LayoutView
    template: templateIndex

    regions:
      chart: ".js-chart"

    onRender: ->
      @showChart 40

    showChart: (quantity) ->
      @chart.show new popularAirportsMapView
        quantity: quantity
        height: 600
