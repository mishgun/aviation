define [
  "Marionette"
  "templates/highest_airports/index"
  "views/highest_airports/chart"
], (
  Marionette
  templateIndex
  highestAirportsChartView
) ->

  class highestAirportsView extends Marionette.LayoutView
    template: templateIndex

    regions:
      chart: ".js-chart"

    onRender: ->
      @showChart 20

    showChart: (quantity) ->
      @chart.show new highestAirportsChartView
        quantity: quantity
        height: 400
