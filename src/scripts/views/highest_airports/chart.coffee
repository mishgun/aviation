define [
  "Marionette"
], (
  Marionette
) ->

  class highestAirportsChartView extends Marionette.ItemView
    className: "chart-wrap"
    template: false

    initialize: (options) ->
      quantity = options.quantity or 5
      @height = options.height or 250
      @data = @getData quantity
      @url = options.url or null

    onShow: ->
      @initChart()

    getData: (quantity=5) ->
      data = AviationStats.Data.airports.getHighestAirports quantity, true
      _.map data, (model, i) ->
        x: i
        y: ~~(model.altitude / 3.2808)
        name: "#{model.city} (#{model.country})"

    initChart: ->
      @$el.highcharts
        chart:
          type: "area"
          height: @height
          backgroundColor: "rgba(255,255,255,0)"
          events:
            click: (event) =>
              AviationStats.layout.redirectUrl @url if @url
        title: false
        xAxis:
          type: 'category'
          labels:
            rotation: -45
        yAxis:
          min: 0
          title: text: 'Altitude, m'
        legend:
          enabled: false
        plotOptions:
          area:
            marker:
              enabled: true
              symbol: 'circle'
              radius: 2
              states:
                hover:
                  enabled: true
        tooltip:
          pointFormat: 'Altitude: <b>{point.y}</b>'
        series: [{
          name: 'Airports'
          data: @data
        }]
