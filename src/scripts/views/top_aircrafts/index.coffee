define [
  "Marionette"
  "templates/top_aircrafts/index"
  "views/top_aircrafts/chart"
], (
  Marionette
  templateIndex
  topAircraftsChartView
) ->

  class topAircraftsView extends Marionette.LayoutView
    template: templateIndex

    regions:
      chart: ".js-chart"

    ui:
      quantity: ".js-quantity"

    events:
      "change @ui.quantity": "onQuantityChange"

    onRender: ->
      @showChart 20

    onQuantityChange: ->
      val = @ui.quantity.val()
      return unless /^\d+$/.test(val)
      return if val < 1 or val > 100

      @chart.currentView.destroy()
      @showChart val

    showChart: (quantity) ->
      @chart.show new topAircraftsChartView
        quantity: quantity
        height: quantity * 20 + 80
