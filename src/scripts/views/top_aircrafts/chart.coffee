define [
  "Marionette"
], (
  Marionette
) ->

  class topAircraftsChartView extends Marionette.ItemView
    className: "chart-wrap"
    template: false

    initialize: (options) ->
      quantity = options.quantity or 5
      @height = options.height or 250
      @data = @getData quantity
      @categories = @getNames quantity
      @url = options.url or null

    onShow: ->
      @initChart()

    getData: (quantity=5) ->
      data = AviationStats.Data.flights.getPopularAircrafts quantity
      _.map data, (model) =>
        y: model.flights
        x: model.aircraft_name
        color: @getRandomColor()

    getNames: (quantity=5) ->
      data = AviationStats.Data.flights.getPopularAircrafts quantity
      _.map data, (model) => model.aircraft_name

    initChart: ->
      @$el.highcharts
        chart:
          type: 'bar'
          height: @height
          backgroundColor: "rgba(255,255,255,0)"
          events:
            click: (event) =>
              AviationStats.layout.redirectUrl @url if @url
        title: false
        legend:
          enabled: false
        plotOptions:
          bar:
            dataLabels:
              enabled: true
        series: [{
          name: "Flights"
          data: @data
        }]
        yAxis:
          min: 0
          title:
            text: "Flights"
            align: "high"
          labels:
            overflow: 'justify'
        xAxis:
          categories: @categories
          title:
            text: null

    getRandomColor: ->
      r = ~~(Math.random() * 250)
      g = ~~(Math.random() * 250)
      b = ~~(Math.random() * 250)
      "rgba(#{r},#{g},#{b},1)"
