define [
  "Marionette"
  "templates/popular_airports/index"
  "views/popular_airports/chart"
], (
  Marionette
  templateIndex
  popularAirportsChartView
) ->

  class topCountriesView extends Marionette.LayoutView
    template: templateIndex

    regions:
      chart: ".js-chart"

    onRender: ->
      @showChart 40

    showChart: (quantity) ->
      @chart.show new popularAirportsChartView
        quantity: quantity
        height: 400
