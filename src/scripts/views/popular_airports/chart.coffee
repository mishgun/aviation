define [
  "Marionette"
  "hcmore"
], (
  Marionette
) ->

  class popularAirportsChartView extends Marionette.ItemView
    className: "chart-wrap"
    template: false

    initialize: (options) ->
      quantity = options.quantity or 5
      @height = options.height or 250
      @data = @getData quantity
      @url = options.url or null

    onShow: ->
      @initChart()

    getData: (quantity=5) ->
      data = AviationStats.Data.flights.getPopularAirports quantity
      _.map data, (model) =>
        x: ~~(Math.random() * 101)
        y: ~~(Math.random() * 101)
        z: model.flights
        name: "#{model.airport_name} (#{model.airport_city}, #{model.airport_country})"
        fillColor:
          radialGradient:
            cx: 0.4
            cy: 0.3
            r: 0.7
          stops: [
            [0, @getRandomColor(0.5)]
            [1, "rgba(100,100,100,1)"]
          ]

    initChart: ->
      @$el.highcharts
        chart:
          type: 'bubble'
          height: @height
          backgroundColor: "rgba(255,255,255,0)"
          events:
            click: (event) =>
              AviationStats.layout.redirectUrl @url if @url
        title: false
        legend:
          enabled: false
        tooltip:
          headerFormat: "<p>{point.key}</p><br />"
          pointFormat: "Flights: <b>{point.z}</b>"
        xAxis:
          labels:
            enabled: false
        yAxis:
          labels:
            enabled: false
          title:
            text: "Flights"
        series: [{
          data: @data
          marker:
            fillColor: {}
          maxSize: "40%"
          minSize: "10%"
        }]

    getRandomColor: (opacity) ->
      r = ~~(Math.random() * 250)
      g = ~~(Math.random() * 250)
      b = ~~(Math.random() * 250)
      "rgba(#{r},#{g},#{b},#{opacity})"
