define [
  "Marionette"
], (
  Marionette
) ->

  class baseChartView extends Marionette.ItemView
    className: "chart-wrap"
    template: false

    initialize: (options) ->
      quantity = options.quantity or 5
      @height = options.height or 250
      @data = @getData quantity
      @url = options.url or null

    onShow: ->
      @initChart()

    initChart: ->
      @$el.highcharts
        chart:
          type: 'column'
          height: @height
          backgroundColor: "rgba(255,255,255,0)"
          events:
            click: (event) =>
              AviationStats.layout.redirectUrl @url if @url
        title: false
        xAxis:
          type: 'category'
          labels:
            rotation: -45
        yAxis:
          min: 0
          title: text: 'Airports'
        legend: enabled: false
        plotOptions:
          series:
            borderWidth: 0
            dataLabels:
              enabled: true
              format: '{point.y}'
              rotation: -90
              y: -15
        tooltip: pointFormat: 'Airports: <b>{point.y}</b>'
        series: [{
          name: 'Airports'
          data: @data
          colorByPoint: true
        }]
