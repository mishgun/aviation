define [
  "Marionette"
  "templates/application/header"
], (
  Marionette
  templateHeader
) ->

  class HeaderView extends Marionette.ItemView
    template: templateHeader