define [
  "Marionette"
  "templates/top_countries/index"
  "views/top_countries/chart"
], (
  Marionette
  templateIndex
  topCountriesChartView
) ->

  class topCountriesView extends Marionette.LayoutView
    template: templateIndex

    regions:
      chart: ".js-chart"

    ui:
      quantity: ".js-quantity"

    events:
      "change @ui.quantity": "onQuantityChange"

    onRender: ->
      @showChart 20

    onQuantityChange: ->
      val = @ui.quantity.val()
      return unless /^\d+$/.test(val)
      return if val < 1 or val > 50

      @chart.currentView.destroy()
      @showChart val

    showChart: (quantity) ->
      @chart.show new topCountriesChartView
        quantity: quantity
        height: 400