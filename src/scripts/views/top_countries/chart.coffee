define [
  "Marionette"
  "views/application/base_chart"
], (
  Marionette
  baseChartView
) ->

  class topCountriesChartView extends baseChartView

    getData: (quantity=5) ->
      data = AviationStats.Data.airports.getTopCountries quantity
      _.map data, (model) -> [model.country, model.airports]
